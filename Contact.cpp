#include <iostream>
using namespace std;

#define MAX_PERSON 100

//欢迎信息
void ShowMenu()
{
	cout << "1.添加联系人\n2.显示联系人\n3.删除联系人\n4.查找联系人\n5.修改联系人\n6.清空联系人" << endl;
}

struct Contact
{
	string name;
	int age;
};

void DisplayContact(Contact* p, int len)
{
	for (int i = 0; i < len; i++)
	{
		cout << p[i].name << "  " << p[i].age << endl;
	}
}

void AddContact(Contact* p, Contact c, int len)
{
	p[len] = c;
}

void DeleteContact(Contact* p, string name, int * len)
{
	for (int i = 0; i < *len; i++)
	{
		if (p[i].name == name)
		{
			for (int j = i; j < *len; j++)
			{
				p[j] = p[j + 1];
			}
			(*len)--;//*与--优先级相同 因此将从右向左执行 因此必须加括号提高优先级 否则指针越界 
		}
	}
}

class MyClass_2
{
public:
	void GetMyClassPrivateMem();
};

class MyClass
{
	friend class MyClass_1; //类做友元
	friend void GetMyClassPrivateMem(MyClass* p); //全局函数做友元
	friend void MyClass_2::GetMyClassPrivateMem();
public:
	MyClass()
	{
		m_num = 0;
	}

	MyClass(int num)
	{
		this->m_num = num;
	}

	MyClass& operator++()
	{
		m_num++;
		return *this;
	}

	MyClass operator++(int)
	{
		MyClass m;
		m = *this;
		m_num++;
		return m;
	}

	MyClass* Add(int num)
	{
		this->m_num += num;
		return this;
	}

	MyClass& Add_1(int num)
	{
		this->m_num += num;
		return *this;
	}

	void ShowNum()
	{
		if (this == NULL) return; //提高代码健壮性 避免空指针调用此函数时程序崩溃
		else cout << "m_num = " << m_num << endl;
	}

	void ChangeNum() const
	{
		//m_num = 6; //常函数不可修改成员变量
	}

	int GetNum()
	{
		return m_num;
	}

private:
	int m_num;
};

class MyClass_1
{
};

void GetMyClassPrivateMem(MyClass* p)
{
	cout << "MyClass的私有成员m_num的值是 " << p->m_num << endl;
}

ostream& operator<< (ostream & cout, MyClass& m)
{
	return cout << m.GetNum() << endl;
	// return cout; // 也可
}

class Animal
{
public:
	virtual void Speak()
	{
		cout << "Animal" << endl;
	}

private:

};

class Dog :public Animal
{
public:
	void Speak()
	{
		cout << "Dog" << endl;
	}
};

class Cat :public Animal
{
public:
	void Speak()
	{
		cout << "Cat" << endl;
	}
};

void Speak(Animal& a) // 父类的指针或引用指向子类对象
{
	a.Speak();
}

int main()
{
	int num = 0;//接收用户输入的值

	MyClass_1 null_p;
	cout << "空对象占用内存空间为" << sizeof(null_p) << endl;
	MyClass p(1);
	cout << "非空对象占用内存空间为" << sizeof(p) << endl;
	MyClass q(1);

	//链式编程思想
	p.Add(5)->Add(5)->Add(5);
	q.Add_1(5).Add_1(5).Add_1(5);
	p.ShowNum();
	q.ShowNum();

	//常对象&常函数
	const MyClass const_p;
	//const_p.ShowNum(); //常对象只能调用常函数
	const_p.ChangeNum();

	//友元
	GetMyClassPrivateMem(&p);

	//重载<<运算符
	cout << "运算符重载： MyClass::m_num值为 " << q;

	//重载前置++&重载后置++
	q++;
	cout << "前置++ = " << q;
	cout << "后置++ = " << ++q;

	Contact arr[MAX_PERSON];
	int contact_num = 0;

	Animal a;
	Speak(a);
	Cat c;
	Speak(c);

	while (true)
	{
		ShowMenu();
		cin >> num;
		switch (num)
		{
		case 1:
			if (contact_num < MAX_PERSON)
			{
				Contact c;
				cout << "输入姓名" << endl;
				cin >> c.name;
				cout << "输入年龄" << endl;
				cin >> c.age;
				AddContact(arr, c, contact_num);
				contact_num++;
			}
			else
			{
				cout << "通讯录已满" << endl;
			}
			break;
		case 2:
			DisplayContact(arr, contact_num);
			break;
		case 3:
		{
			cout << "输入姓名" << endl;
			string name;
			cin >> name;
			DeleteContact(arr, name, &contact_num);
		}
			break;
		default:
			system("cls");
			break;
		}
	}

	system("pause");
	return 0;
}